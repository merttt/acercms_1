﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcerCMS.Filters
{
    public class ShowMessageAttribute : FilterAttribute, IResultFilter
    {
        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (filterContext.Controller.TempData["Message"] != null)
            {
                filterContext.Controller.ViewBag.Message = filterContext.Controller.TempData["Message"];
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            
        }
    }
}